package vc_stats

import (
  "fmt"
  "sync"
  "time"

  "github.com/bwmarrin/discordgo"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/service/cloudwatch"
)

var (
  users_lock = &sync.Mutex{}
  // Map of user tags (name#1234)
  users_online = make(map[string]struct{})
)

const COLA = "803542061977960448"

func AddHandlers(s *discordgo.Session) {
  guild, err := s.State.Guild(COLA)
  fmt.Println(err)
  fmt.Println(guild.Members)
  go send_metrics()
  s.AddHandler(func(s *discordgo.Session, u *discordgo.VoiceStateUpdate) {
    // We only care about cola
    if u.GuildID != COLA {
      return
    }
    user, err := s.User(u.UserID)
    if err != nil {
      fmt.Println(err)
      return
    }
    users_lock.Lock()
    defer users_lock.Unlock()
    if u.ChannelID == "" {
      _, ok := users_online[user.String()]
      if ok {
        delete(users_online, user.String())
      }
    } else {
      users_online[user.String()] = struct{}{}
    }
  })
}

func send_metrics() {
  sess := session.Must(session.NewSession(aws.NewConfig().WithRegion("us-west-2")))
  c := cloudwatch.New(sess)

  t := time.Now().Truncate(time.Minute).Add(time.Minute)
  time.Sleep(time.Until(t))
  send(c)
  ticker := time.NewTicker(time.Minute)
  for range ticker.C {
    send(c)
  }
}

func send(c *cloudwatch.CloudWatch) {
  users_lock.Lock()
  t := time.Now()
  val := float64(len(users_online))
  users_lock.Unlock()
  _, err := c.PutMetricData(&cloudwatch.PutMetricDataInput{
    Namespace: aws.String("cola/stats"),
    MetricData: []*cloudwatch.MetricDatum{
      &cloudwatch.MetricDatum{
        MetricName: aws.String("vc"),
        Timestamp: &t,
        Value: &val,
      },
    },
  })
  if err != nil {
    fmt.Println(err)
  }
}
