package main

import (
  "os"
  "fmt"
  "time"
  "syscall"
  "strings"
  "os/signal"
  "io/ioutil"

  "github.com/bwmarrin/discordgo"

  "gitlab.com/macmv/cola-bot/afk"
  "gitlab.com/macmv/cola-bot/ytp"
  "gitlab.com/macmv/cola-bot/coins"
  "gitlab.com/macmv/cola-bot/vc_stats"
  "gitlab.com/macmv/cola-bot/role_colors"
  "gitlab.com/macmv/cola-bot/role_storage"
  "gitlab.com/macmv/cola-bot/util_commands"
)

func main() {
  token, err := ioutil.ReadFile("token.txt")
  if err != nil { panic(err) }

  token_str := strings.TrimSpace(string(token))

  s, err := discordgo.New("Bot " + token_str)
  if err != nil { panic(err) }
  defer s.Close()

  err = s.Open()
  if err != nil { panic(err) }

  fmt.Println("Logged in! Adding handlers...")

  util_commands.AddHandlers(s)
  role_storage.AddHandlers(s)
  role_colors.AddHandlers(s)
  vc_stats.AddHandlers(s)
  coins.AddHandlers(s)
  afk.AddHandlers(s)
  ytp.AddHandlers(s)
  AddHandlers(s)

  fmt.Println("Done with handlers")

  c := make(chan os.Signal)
  signal.Notify(c, os.Interrupt, syscall.SIGTERM)
  go func() {
    <-c
    fmt.Println("\nClosing...")
    s.Close()
    os.Exit(0)
  }()

  // Wait here forever
  for {
    time.Sleep(10 * time.Second)
  }
}
