package audio

import (
  "math"
)

// This returns a sample_func which plays a constant sin wave, given a note
// in hz.
func NoteSampler(note float64) SampleFunc {
  return func(samplerate, num_samples, time int64) ([]int16, error) {
    data := make([]int16, num_samples)
    for i := int64(0); i < num_samples; i++ {
      data[i] = int16(math.Sin((note * float64(time + i) * 2 * math.Pi) / float64(samplerate)) * 16384)
    }
    return data, nil
  }
}
