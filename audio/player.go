package audio

import (
  "log"

  "github.com/bwmarrin/discordgo"

  "gopkg.in/hraban/opus.v2"
)

// This is used in play_audio, and is called once every 20 millis.
type SampleFunc func(samplerate, num_samples, time int64) ([]int16, error)

// This function first connects the bot to voice,
// and then calls sample_func every 20 millis,
// until sample_func returns either an error or a nil array.
// This is a blocking call.
func Play(vc *discordgo.VoiceConnection, sample_func SampleFunc, skip chan struct{}) {
  // This is the sample rate to use with opus encoding
  samplerate := int64(48000)

  // Used to encode audio
  enc, err := opus.NewEncoder(int(samplerate), 1, opus.AppVoIP)
  if err != nil { log.Println("Failed to create opus encoder:", err); return }

  // This is the total number of samples that have been covered
  t := int64(0)
  for {
    // This means the bot has not fully connected
    if !vc.Ready {
      continue
    }

    // Raw pcm data
    // Opus uses 20 ms samples, so we divide the samplerate by 50
    num_samples := samplerate / 50
    pcm, err := sample_func(samplerate, num_samples, t)
    if pcm == nil {
      break
    }
    if err != nil {
      log.Println(err)
      break
    }
    t += num_samples

    // Opus encoded data
    data := make([]byte, len(pcm))
    size, err := enc.Encode(pcm, data)
    if err != nil {
      log.Println("Failed to encode pcm data:", err)
      break
    }

    // This will block if it doesn't need a sample
    select {
    case vc.OpusSend <- data[:size]:
    case <- skip:
      return
    }
  }
}
