package audio

import (
  "bytes"
  "encoding/binary"
)

func WavSampler(wav *bytes.Buffer) SampleFunc {
  // This is the wav file header
  header := wav.Next(44)
  //log.Println("Audio format:", binary.LittleEndian.Uint16(header[20:]))
  //log.Println("Number of channels:", binary.LittleEndian.Uint16(header[22:]))
  //log.Println("Sample rate for wav:", binary.LittleEndian.Uint32(header[24:]))
  //log.Println("Block align:", binary.LittleEndian.Uint16(header[32:]))
  //log.Println("Bits per sample:", binary.LittleEndian.Uint16(header[34:]))
  // This is the total number of bytes per each sample,
  // including channels. So if we have stero audio,
  // at 16 bits per sample, then we want to read the first
  // two bytes of a 4 byte section to get mono audio.
  block_size := int(binary.LittleEndian.Uint16(header[32:]))

  data := make([]int16, (wav.Len() - 44) / block_size)
  for i := int64(0); i < int64(len(data)); i++ {
    data[i] = int16(binary.LittleEndian.Uint16(wav.Next(block_size)))
  }

  return func(samplerate, num_samples, time int64) ([]int16, error) {
    if time + num_samples >= int64(len(data)) {
      return nil, nil
    }
    return data[time:time+num_samples], nil
  }
}
