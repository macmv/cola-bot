package role_colors

import (
  // "fmt"
  // "sort"

  "github.com/bwmarrin/discordgo"
)

const COLA = "803542061977960448"
const PERM_ANNOUCE = "841125451215667220"
const MSG = "841126031560540181"

var role_names = map[string]string{
  "red": "841130827097571409",
  "orange": "841130828682887208",
  "yellow": "841130829613629483",
  "green": "841130831048343572",
  "blue": "841130832147775499",
  "purple": "841130833805049897",
  "white": "841130835650543637",
  "black": "841130837146599434",
}

func AddHandlers(s *discordgo.Session) {
  // reactions := []string{
  //   "🟥", // Red
  //   "🟧", // Orange
  //   "🟨", // Yellow
  //   "🟩", // Green
  //   "🟦", // Blue
  //   "🟪", // Purple
  //   "⬜", // White
  //   "⬛", // Black
  // }
  // names := []string{
  //   "red",
  //   "orange",
  //   "yellow",
  //   "green",
  //   "blue",
  //   "purple",
  //   "white",
  //   "black",
  // }
  // for _, name := range names {
  //   id := role_names[name]
  //   s.GuildRoleEdit(COLA, id, name + "-pilled", 0xff0000, false, 0, false)
  //   // s.MessageReactionAdd(PERM_ANNOUCE, MSG, emoji)
  //   // role, _ := s.GuildRoleCreate(COLA)
  //   // role, _ = s.GuildRoleEdit(COLA, role.ID, names[i] + "-pilled", 0, false, 0, false)
  //   // fmt.Println("\"" + names[i] + "-pilled\": \"" + role.ID + "\"")
  // }
  // current_roles, _ := s.GuildRoles(COLA)
  // sort.Slice(current_roles, func(i, j int) bool {
  //   return current_roles[i].Position < current_roles[j].Position
  // })
  // for i := len(current_roles) - 1; i >= 0; i-- {
  //   role := current_roles[i]
  //   fmt.Println(role.ID, role.Name, role.Permissions & 0x00000008)
  // }
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
    if m.MessageID != MSG {
      return
    }
    name := ""
    switch m.Emoji.Name {
      case "🟥": name = "red"
      case "🟧": name = "orange"
      case "🟨": name = "yellow"
      case "🟩": name = "green"
      case "🟦": name = "blue"
      case "🟪": name = "purple"
      case "⬜": name = "white"
      case "⬛": name = "black"
    }
    id, ok := role_names[name]
    if !ok {
      return
    }
    s.GuildMemberRoleAdd(COLA, m.UserID, id)
  })
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageReactionRemove) {
    if m.MessageID != MSG {
      return
    }
    name := ""
    switch m.Emoji.Name {
      case "🟥": name = "red"
      case "🟧": name = "orange"
      case "🟨": name = "yellow"
      case "🟩": name = "green"
      case "🟦": name = "blue"
      case "🟪": name = "purple"
      case "⬜": name = "white"
      case "⬛": name = "black"
    }
    id, ok := role_names[name]
    if !ok {
      return
    }
    s.GuildMemberRoleRemove(COLA, m.UserID, id)
  })
}
