package coins

// import (
//   "fmt"
//   "math"
//   "sort"
// 
//   "github.com/bwmarrin/discordgo"
// )
// 
// // This locks bals, and sends money from one user to another.
// func pay(from, to *discordgo.User, amount coins) string {
//   bals_lock.Lock()
//   defer bals_lock.Unlock()
//   loans_lock.Lock()
//   defer loans_lock.Unlock()
//   loan_pay_amount := amount
//   changed_loans := false
//   for i, l := range loans_received[from.ID] {
//     if l.from == to.ID {
//       changed_loans = true
//       // This loan is between to and from, so this pay() should pay back part of this
//       // loan.
//       if loan_pay_amount < l.amount {
//         l.amount -= loan_pay_amount
//         loan_pay_amount = 0
//       } else {
//         loan_pay_amount -= l.amount
//         l.amount = 0
//       }
//       if l.amount <= 0 {
//         list := loans_received[from.ID]
//         copy(list[i:], list[i+1:])
//         loans_received[from.ID] = list[:len(list)-1]
//         for j, other := range loans_sent[to.ID] {
//           if other == l {
//             list := loans_sent[to.ID]
//             copy(list[j:], list[j+1:])
//             loans_sent[to.ID] = list[:len(list)-1]
//           }
//         }
//       }
//       if loan_pay_amount <= 0 {
//         break
//       }
//     }
//   }
//   if changed_loans {
//     save_loans()
//   }
//   valid := add_bal(from.ID, -amount)
//   if !valid {
//     return fmt.Sprintf(
//       "%v, you are too poor! You tried to pay %v, but you only have %v",
//       from.Mention(),
//       amount,
//       get_bal(from.ID),
//     )
//   }
//   add_bal(to.ID, amount)
//   save_bals()
//   return fmt.Sprintf(
//     "%v payed %v %v",
//     from.Mention(),
//     to.Mention(),
//     amount,
//   )
// }
// 
// // This locks bals, and creates a loan from one user to another. Intrest is payed
// // once every hour. You may pay back a loan before any interest is applied.
// func loan(from, to *discordgo.User, amount coins, interest float64) string {
//   bals_lock.Lock()
//   defer bals_lock.Unlock()
//   valid := add_bal(from.ID, -amount)
//   if !valid {
//     return fmt.Sprintf(
//       "%v, you are too poor! You tried to loan %v, but you only have %v",
//       from.Mention(),
//       amount,
//       get_bal(from.ID),
//     )
//   }
//   add_bal(to.ID, amount)
//   save_bals()
//   loans_lock.Lock()
//   defer loans_lock.Unlock()
//   loan := &outstanding_loan{
//     from: from.ID,
//     to: to.ID,
//     amount: amount,
//     interest: interest,
//   }
//   loans_sent[from.ID] = append(loans_sent[from.ID], loan)
//   sort.Slice(loans_sent[from.ID], func(i, j int) bool {
//     return loans_sent[from.ID][i].interest > loans_sent[from.ID][j].interest
//   })
//   loans_received[to.ID] = append(loans_received[to.ID], loan)
//   sort.Slice(loans_received[to.ID], func(i, j int) bool {
//     return loans_received[to.ID][i].interest > loans_received[to.ID][j].interest
//   })
//   save_loans()
//   return fmt.Sprintf(
//     "%v loaned %v %v",
//     from.Mention(),
//     to.Mention(),
//     amount,
//   )
// }
// 
// // This locks bals, and gives everyone interest. Amount is a percentage
// // as a float, so for 2% interest, pass in 0.02. The returned list of strings
// // is a list of discord messages.
// func interest(amount float64) []string {
//   bals_lock.Lock()
//   defer bals_lock.Unlock()
//   multiplier := amount + 1
//   for id, b := range bals {
//     bals[id] = coins(math.Ceil(float64(b) * multiplier))
//   }
//   save_bals()
//   loans_lock.Lock()
//   defer loans_lock.Unlock()
//   strs := []string{}
//   for id, list := range loans_received {
//     total_owed := coins(0)
//     for _, l := range list {
//       l.amount = coins(math.Ceil(float64(l.amount) * l.interest))
//       total_owed += l.amount
//     }
//     if total_owed * 2 > get_bal(id) {
//       fmt.Println(id, "is bankrupt!")
//       for _, l := range list {
//         add_bal(l.from, l.amount)
//         // This means they would have no money, so we just generate some extra money from the void.
//         if !add_bal(l.to, -l.amount) {
//           bals[l.to] = 1
//         }
// 
//         other_list := loans_sent[l.from]
//         for i, other_loan := range other_list {
//           if other_loan == l {
//             copy(other_list[i:], other_list[i+1:])
//             loans_sent[l.from] = other_list[:len(other_list)-1]
//           }
//         }
//       }
//       loans_received[id] = nil
//       u := &discordgo.User{ID: id}
//       strs = append(strs, fmt.Sprintf(
//         "%v went bankrupt! All loans have been payed back",
//         u.Mention(),
//       ))
//     }
//   }
//   save_loans()
//   strs = append(strs, fmt.Sprintf(
//     "Gave everyone %.2f%% interest",
//     amount * 100,
//   ))
//   return strs
// }
