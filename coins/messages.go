package coins

import (
  "fmt"

  "github.com/bwmarrin/discordgo"
)

func pay_request(s *discordgo.Session, from, to *discordgo.User, amount coins) {
  msg, err := s.ChannelMessageSend(TRANSACTIONS_CHANNEL, fmt.Sprintf(
    "%v, are you sure you want to pay %v %v?",
    from.Mention(),
    to.Mention(),
    amount,
  ))
  if err != nil {
    fmt.Println("ERROR: Could not send message:", err)
    return
  }
  s.MessageReactionAdd(msg.ChannelID, msg.ID, ACCEPT_ID)
  s.MessageReactionAdd(msg.ChannelID, msg.ID, DECLINE_ID)
  pending_lock.Lock()
  pending[msg.ID] = &pending_transaction{
    t: Transaction_PAY,
    from: from,
    to: to,
    amount: amount,
  }
  pending_lock.Unlock()
}

func send_help(s *discordgo.Session, channel, message string) {
  msg, err := s.ChannelMessageSend(channel, message)
  if err == nil {
    s.MessageReactionAdd(msg.ChannelID, msg.ID, OK_ID)
    ok_lock.Lock()
    ok_messages[msg.ID] = struct{}{}
    ok_lock.Unlock()
  }
}
