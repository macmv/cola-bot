package coins

import (
  "os"
  "fmt"
  "strings"
  "strconv"

  f "github.com/fauna/faunadb-go/v3/faunadb"
)

// This transferes balances to faunadb, from the old text file.
func transfer_bals() {
  s, err := os.ReadFile("cache/bals.txt")
  if err != nil {
    fmt.Println("Could not read bals.txt!")
    return
  }
  lines := strings.Split(string(s), "\n")
  bals := make(map[string]coins)
  for i, l := range lines {
    if l == "" {
      continue
    }
    sections := strings.Split(l, ": ")
    if len(sections) != 2 {
      fmt.Printf("WARNING: Invalid balance file! Line %v is not formatted correctly\n", i)
      continue
    }
    amount, err := strconv.ParseInt(sections[1], 10, 64)
    if err != nil {
      fmt.Printf("WARNING: Invalid balance file! Line %v is not formatted correctly: %v\n", i, err)
      continue
    }
    bals[sections[0]] = coins(amount)
  }
  exprs := []f.Expr{}
  for user, amount := range bals {
    exprs = append(exprs, f.Create(
      f.Collection("balances"),
      f.Obj{
        "data": f.Obj{
          "user": user,
          "amount": amount,
        },
      },
    ))
  }
  res, err := fauna_client.Query(f.Do(exprs))
  fmt.Println(res, err)
}
// Need to rewrite this
// s, err = os.ReadFile("cache/loans.txt")
// if err != nil {
//   save_loans()
// } else {
//   lines := strings.Split(string(s), "\n")
//   loans_lock.Lock()
//   defer loans_lock.Unlock()
//   for i, l := range lines {
//     if l == "" {
//       continue
//     }
//     sections := strings.Split(l, ": ")
//     if len(sections) != 2 {
//       fmt.Printf("WARNING: Invalid loans file! Line %v is not formatted correctly\n", i + 1)
//       continue
//     }
//     users := strings.Split(sections[0], " ")
//     if len(users) != 2 {
//       fmt.Printf("WARNING: Invalid loans file! Line %v is not formatted correctly\n", i + 1)
//       continue
//     }
//     amounts := strings.Split(sections[1], " ")
//     if len(amounts) != 2 {
//       fmt.Printf("WARNING: Invalid loans file! Line %v is not formatted correctly\n", i + 1)
//       continue
//     }
//     from := users[0]
//     to := users[1]
//     amount, err := strconv.ParseInt(amounts[0], 10, 64)
//     if err != nil {
//       fmt.Printf("WARNING: Invalid loans file! Line %v is not formatted correctly: %v\n", i + 1, err)
//       continue
//     }
//     interest, err := strconv.ParseFloat(amounts[1], 64)
//     if err != nil {
//       fmt.Printf("WARNING: Invalid loans file! Line %v is not formatted correctly: %v\n", i + 1, err)
//       continue
//     }
//     loan := &outstanding_loan{
//       from: from,
//       to: to,
//       amount: coins(amount),
//       interest: interest,
//     }
//     loans_sent[from] = append(loans_sent[from], loan)
//     sort.Slice(loans_sent[from], func(i, j int) bool {
//       return loans_sent[from][i].interest > loans_sent[from][j].interest
//     })
//     loans_received[to] = append(loans_received[to], loan)
//     sort.Slice(loans_received[to], func(i, j int) bool {
//       return loans_received[to][i].interest > loans_received[to][j].interest
//     })
//   }
// }

// // This does not lock bals! It saves everyone's balance to disk.
// func save_bals() {
//   f, err := os.Create("cache/bals.txt")
//   if err != nil {
//     panic("Error while created balances: " + err.Error())
//   }
//   defer f.Close()
//   for id, amount := range bals {
//     f.WriteString(fmt.Sprintf("%v: %d\n", id, amount))
//   }
// }
//
// // This does not lock loans! It saves all loans to disk
// func save_loans() {
//   f, err := os.Create("cache/loans.txt")
//   if err != nil {
//     panic("Error while created balances: " + err.Error())
//   }
//   defer f.Close()
//   for _, list := range loans_sent {
//     for _, l := range list {
//       f.WriteString(fmt.Sprintf(
//         "%v %v: %d %.2f\n",
//         l.from,
//         l.to,
//         l.amount,
//         l.interest,
//       ))
//     }
//   }
// }
