package coins

import (
  "os"
  "io"
  "log"
  "sync"
  "bufio"
  "strings"
  "strconv"
  "net/http"
  "encoding/json"
  "github.com/google/uuid"

  "github.com/gorilla/mux"

  "github.com/bwmarrin/discordgo"
)

// Global ip. Sent whenever someone dm's the bot 'api'.
var ip string
// Discord session
var s *discordgo.Session

// Used for neeleman to create cc when needed.
var admin_token string
// Used on the minecraft server to take out vouchers.
var minecraft_token string
// Used to lock the vouchers file
var vouchers_lock *sync.Mutex

func serve_api(sess *discordgo.Session) {
  s = sess
  // Aws public ipv4 address
  res, err := http.Get("http://169.254.169.254/latest/meta-data/public-ipv4")
  if err != nil {
    panic("Failed to get public ipv4 address: " + err.Error())
  }
  ip_bytes, err := io.ReadAll(res.Body)
  if err != nil {
    panic("Failed to get public ipv4 address: " + err.Error())
  }
  res.Body.Close()
  ip = string(ip_bytes) + ":8000"

  r := mux.NewRouter()
  // This is the main structure of the api. See the handle functions for more info.
  r.HandleFunc("/coins/top", handle_top)
  r.HandleFunc("/coins/get/{user}", handle_get)
  r.HandleFunc("/coins/pay/{from}/{to}/{amount:[0-9]+}", handle_pay)
  r.HandleFunc("/coins/admin/add/{user}/{amount:[0-9]+}", handle_add)

  // For the minecraft mod
  r.HandleFunc("/coins/get_minecraft/{user}", handle_get_minecraft)
  r.HandleFunc("/coins/voucher/create/{minecraft_user}/{amount:[0-9]+}", handle_create_voucher)
  r.HandleFunc("/coins/voucher/redeem/{minecraft_user}/{voucher}", handle_redeem_voucher)

  http.Handle("/", r)
  go func() {
    log.Fatal(http.ListenAndServeTLS(":8000", "colacoin.crt", "colacoin.key", nil))
  }()

  token, err := os.ReadFile("admin_token.txt")
  if err != nil {
    panic("Failed to read admin token: " + err.Error())
  }
  admin_token = strings.TrimSpace(string(token))

  token, err = os.ReadFile("minecraft_token.txt")
  if err != nil {
    panic("Failed to read minecraft token: " + err.Error())
  }
  minecraft_token = strings.TrimSpace(string(token))

  vouchers_lock = &sync.Mutex{}
}

// This takes out an amount of colacoin from their balance,
// and returns a unique token that can be used later to add
// to someone else's balance.
func handle_create_voucher(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  r.ParseForm()
  token, ok := r.Form["secret"]
  var out map[string]interface{}
  if !ok || len(token) != 1 {
    out = map[string]interface{}{
      "error": "no secret",
      "success": false,
    }
  } else if token[0] == minecraft_token {
    user := lookup_minecraft_user(mux.Vars(r)["minecraft_user"])
    if user == "" {
      out = map[string]interface{}{
        "error": "unknown user",
        "success": false,
      }
      json.NewEncoder(w).Encode(out)
      return
    }
    amount, _ := strconv.Atoi(mux.Vars(r)["amount"])
    if amount < 1 {
      out = map[string]interface{}{
        "error": "invalid amount",
        "success": false,
      }
      json.NewEncoder(w).Encode(out)
      return
    }
    id, err := uuid.NewRandom()
    voucher := id.String()
    if len(voucher) != 36 {
      log.Println("Error while creating uuid. Invalid length: " + voucher + ", err: " + err.Error())
      out = map[string]interface{}{
        "error": "internal error",
        "success": false,
      }
      json.NewEncoder(w).Encode(out)
      return
    }

    bal, worked := create_voucher(user, voucher, amount)

    if worked {
      out = map[string]interface{}{
        "voucher": voucher,
        "success": true,
      }
    } else {
      out = map[string]interface{}{
        "bal": bal,
        "error": "too poor",
        "success": false,
      }
    }
  } else {
    out = map[string]interface{}{
      "error": "invalid secret",
      "success": false,
    }
  }
  json.NewEncoder(w).Encode(out)
}

func handle_redeem_voucher(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  r.ParseForm()
  token, ok := r.Form["secret"]
  var out map[string]interface{}
  if !ok || len(token) != 1 {
    out = map[string]interface{}{
      "error": "no secret",
      "success": false,
    }
  } else if token[0] == minecraft_token {
    user := lookup_minecraft_user(mux.Vars(r)["minecraft_user"])
    if user == "" {
      out = map[string]interface{}{
        "error": "unknown user",
        "success": false,
      }
      json.NewEncoder(w).Encode(out)
      return
    }
    voucher := mux.Vars(r)["voucher"]

    worked := redeem_voucher(user, voucher)

    if worked {
      out = map[string]interface{}{
        "success": true,
      }
    } else {
      out = map[string]interface{}{
        "error": "invalid voucher",
        "success": false,
      }
    }
  } else {
    out = map[string]interface{}{
      "error": "invalid secret",
      "success": false,
    }
  }
  json.NewEncoder(w).Encode(out)
}

func handle_get_minecraft(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  user := lookup_minecraft_user(mux.Vars(r)["user"])
  var out map[string]interface{};
  if user == "" {
    out = map[string]interface{}{
      "error": "unknown user",
      "success": false,
    }
  } else {
    bal, ok := get_noset(user)
    out = map[string]interface{}{
      "user": user,
      "amount": bal,
      "known": ok,
      "success": true,
    }
  }
  json.NewEncoder(w).Encode(out)
}

func lookup_minecraft_user(minecraft_user string) string {
  file, err := os.Open("minecraft_usernames.txt")
  if err != nil {
    log.Fatalf("failed to open file: %s", err)
  }

  scanner := bufio.NewScanner(file)
  scanner.Split(bufio.ScanLines)

  for scanner.Scan() {
    text := scanner.Text()
    sections := strings.Split(text, ": ")
    if len(sections) != 2 {
      log.Println("Invalid text in minecraft usernames: ", sections)
      continue
    }
    if sections[0] == minecraft_user {
      file.Close()
      return sections[1]
    }
  }

  file.Close()

  return ""
}

func handle_add(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  r.ParseForm()
  log.Println(r.Form)
  token, ok := r.Form["token"]
  var out map[string]interface{}
  if !ok || len(token) != 1 {
    out = map[string]interface{}{
      "error": "no token",
      "success": false,
    }
  } else if token[0] == admin_token {
    out = map[string]interface{}{
      "success": true,
    }
  } else {
    out = map[string]interface{}{
      "error": "invalid token",
      "success": false,
    }
  }
  json.NewEncoder(w).Encode(out)
}

func handle_top(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  out := []map[string]interface{}{}
  for _, user := range get_top() {
    out = append(out, map[string]interface{}{
      "user": user.User,
      "amount": user.Amount,
    })
  }
  json.NewEncoder(w).Encode(out)
}

func handle_get(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  user := mux.Vars(r)["user"]
  bal, ok := get_noset(user)
  out := map[string]interface{}{
    "user": user,
    "amount": bal,
    "known": ok,
    "success": true,
  }
  json.NewEncoder(w).Encode(out)
}

func handle_pay(w http.ResponseWriter, r *http.Request) {
  w.Header().Add("Content-Type", "application/json")
  vars := mux.Vars(r)
  from_id := vars["from"]
  to_id := vars["to"]
  amount, err := strconv.ParseInt(vars["amount"], 10, 64)
  if err != nil {
    out := make(map[string]interface{})
    out["error"] = err.Error()
    out["success"] = false
    json.NewEncoder(w).Encode(out)
    return
  }
  from, err := s.User(from_id)
  if err != nil {
    out := make(map[string]interface{})
    out["error"] = err.Error()
    out["success"] = false
    json.NewEncoder(w).Encode(out)
    return
  }
  to, err := s.User(to_id)
  if err != nil {
    out := make(map[string]interface{})
    out["error"] = err.Error()
    out["success"] = false
    json.NewEncoder(w).Encode(out)
    return
  }
  pay_request(s, from, to, coins(amount))
  out := map[string]interface{}{}
  out["success"] = true
  json.NewEncoder(w).Encode(out)
}
