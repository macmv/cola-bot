package coins

import (
  "os"
  "fmt"
  "time"
  "sync"
  "strings"
  "strconv"

  "github.com/bwmarrin/discordgo"

  f "github.com/fauna/faunadb-go/v3/faunadb"
)

type transaction_type int

const (
  Transaction_UNKNOWN transaction_type = iota
  Transaction_PAY
  Transaction_LOAN
)

type pending_transaction struct {
  // Type of transaction
  t transaction_type
  // User the money is coming from/going to
  from *discordgo.User
  to   *discordgo.User
  // Only used in loans; stored so that both people must accept the transaction.
  from_accepted bool
  to_accepted bool
  // Amount to pay
  amount coins
  // Intrest amount (used for loans)
  interest float64
}

type outstanding_loan struct {
  // User ids
  from string
  to   string
  // Total amount that needs to be paid back. Will be increase
  // by interest every innterval.
  amount coins
  // Intrest rate as a multiplier (2% interest would be 1.02)
  interest float64
}

var (
  fauna_client *f.FaunaClient
  // Map of message ids to pending transactions
  pending = make(map[string]*pending_transaction)
  pending_lock = &sync.Mutex{}
  // Map of message ids to messages that can be deleted with ok
  ok_messages = make(map[string]struct{})
  ok_lock = &sync.Mutex{}
)

const (
  TRANSACTIONS_CHANNEL string = "818736444835233792"
  STARTING_AMOUNT coins = 1000
  // 0.1% interest every hour
  INTEREST_PER_HOUR float64 = 0.001
  INTEREST_INTERVAL time.Duration = time.Hour
  // Emoji ids
  ACCEPT_ID string = "🚰" // Potable water
  DECLINE_ID string = "🚱" // Non potable water
  OK_ID string = "🆗" // OK Emoji
)

func handle_dm(s *discordgo.Session, m *discordgo.MessageCreate) {
  if m.Content == "api" {
    s.ChannelMessageSend(m.ChannelID, ip)
  }
}

func AddHandlers(s *discordgo.Session) {
  token, err := os.ReadFile("fauna-token.txt")
  if err != nil { panic(err) }
  fauna_client = f.NewFaunaClient(strings.TrimSpace(string(token)))
  // transfer_bals()
  serve_api(s)
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageReactionRemove) {
    // Ignore self reactions
    if m.UserID == s.State.User.ID {
      return
    }
    // Ignore messages outside of #transactions
    if m.ChannelID != TRANSACTIONS_CHANNEL {
      return
    }
    if m.Emoji.Name == ACCEPT_ID {
      pending_lock.Lock()
      t, ok := pending[m.MessageID]
      if ok {
        switch t.t {
        case Transaction_LOAN:
          if m.UserID == t.from.ID {
            t.from_accepted = false
          }
          if m.UserID == t.to.ID {
            t.to_accepted = false
          }
        }
      }
      pending_lock.Unlock()
    }
  })
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
    // Ignore self reactions
    if m.UserID == s.State.User.ID {
      return
    }
    // Ignore messages outside of #transactions
    if m.ChannelID != TRANSACTIONS_CHANNEL {
      return
    }
    if m.Emoji.Name == ACCEPT_ID {
      pending_lock.Lock()
      t, ok := pending[m.MessageID]
      if ok {
        msg := ""
        succeeded := false
        switch t.t {
        case Transaction_PAY:
          if t.from.ID == m.UserID {
            msg, succeeded = pay(t.from.ID, t.to.ID, t.amount)
            if succeeded {
              s.ChannelMessageSend("848068266843701248", fmt.Sprint("paid ", t.from.ID, " ", t.to.ID, " ", t.amount))
            }
          }
        // case Transaction_LOAN:
        //   if m.UserID == t.from.ID {
        //     t.from_accepted = true
        //   }
        //   if m.UserID == t.to.ID {
        //     t.to_accepted = true
        //   }
        //   if t.from_accepted && t.to_accepted {
        //     msg = loan(t.from, t.to, t.amount, t.interest)
        //   }
        default:
          fmt.Println("Unknown transaction type:", t.t)
        }
        if msg != "" {
          delete(pending, m.MessageID)
          s.ChannelMessageEdit(m.ChannelID, m.MessageID, msg)
          s.MessageReactionsRemoveAll(m.ChannelID, m.MessageID)
        }
      }
      pending_lock.Unlock()
    } else if m.Emoji.Name == DECLINE_ID {
      pending_lock.Lock()
      t, ok := pending[m.MessageID]
      if ok && t.from.ID == m.UserID {
        delete(pending, m.MessageID)
        s.ChannelMessageDelete(m.ChannelID, m.MessageID)
      }
      pending_lock.Unlock()
    } else if m.Emoji.Name == OK_ID {
      ok_lock.Lock()
      _, ok := ok_messages[m.MessageID]
      if ok {
        // Some messages are warnings, where you can click ok
        // to get rid of them.
        delete(ok_messages, m.MessageID)
        s.ChannelMessageDelete(m.ChannelID, m.MessageID)
      }
      ok_lock.Unlock()
    }
  })
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
    // Is a dm
    if m.GuildID == "" {
      handle_dm(s, m)
      return
    }
    // Ignore messages outside of #transactions
    if m.ChannelID != TRANSACTIONS_CHANNEL {
      return
    }
    // Ignore self messages
    if m.Author.ID == s.State.User.ID {
      return
    }
    s.ChannelMessageDelete(m.ChannelID, m.ID)
    command := m.Content
    if strings.HasPrefix(command, "!") {
      send_help(s, m.ChannelID, "You don't need the ! at the start of your command (nerd)")
      command = m.Content[1:]
    }
    args := strings.Split(command, " ")
    if len(args) < 1 {
      return
    }
    command = args[0]
    args = args[1:]
    for i := 0; i < len(args); i++ {
      if args[i] == "" {
        copy(args[i:], args[i+1:])
        args = args[:len(args)-1]
        i--
      } else {
        args[i] = strings.ToLower(args[i])
      }
    }
    switch command {
    case "help", "h":
      e := &discordgo.MessageEmbed{}
      e.Type = discordgo.EmbedTypeRich
      e.Title = "ColaCoins Help"
      e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
        Name: "bal [@user]",
        Value: "Shows the balance of the sender, or of a given user",
      })
      e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
        Name: "pay <@user> <amount>",
        Value: "Sends amount to user. If that user has loaned you any money, it will pay back those loans.",
      })
      e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
        Name: "loan <@user> <amount> <interest>",
        Value: "Loans amount to user, with the given interest rate per hour",
      })
      e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
        Name: "loans [@user]",
        Value: "Shows the sent and recieved loans for the given user, or yourself if no user is given",
      })
      e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
        Name: "top",
        Value: "Shows the top users",
      })
      s.ChannelMessageSendEmbed(m.ChannelID, e)
    case "top", "t":
      e := &discordgo.MessageEmbed{}
      e.Type = discordgo.EmbedTypeRich
      e.Title = "Top Users"
      for i, id := range get_top() {
        e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
          Name: fmt.Sprintf("%v", id.Amount),
          Value: fmt.Sprintf("%d. <@%v>", i + 1, id.User),
          Inline: true,
        })
      }
      s.ChannelMessageSendEmbed(m.ChannelID, e)
    case "total", "tot":
      total := get_total()
      s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("The total amount of ColaCoin in circulation is %v", total))
    case "bal", "b":
      if len(args) > 1 {
        send_help(s, m.ChannelID, "Usage: bal [@user]")
        return
      }
      target := m.Author
      if len(m.Mentions) == 1 {
        target = m.Mentions[0]
      }
      s.ChannelMessageSend(m.ChannelID, show_bal(target.ID))
    // case "loans":
    //   if len(args) > 1 {
    //     send_help(s, m.ChannelID, "Usage: loans [@user]")
    //     return
    //   }
    //   target := m.Author
    //   if len(m.Mentions) == 1 {
    //     target = m.Mentions[0]
    //   }
    //   _, err := s.ChannelMessageSendEmbed(m.ChannelID, show_loans(target))
    //   if err != nil {
    //     fmt.Println("ERROR: Could not send message:", err)
    //   }
    case "pay", "p":
      if len(args) != 2 {
        send_help(s, m.ChannelID, "Usage: pay <@user> <amount>")
        return
      }
      if len(m.Mentions) != 1 {
        send_help(s, m.ChannelID, "Usage: pay <@user> <amount>")
        return
      }
      target := m.Mentions[0]
      amount, err := strconv.ParseInt(args[1], 0, 64)
      if err != nil {
        send_help(s, m.ChannelID, "Usage: pay <@user> <amount>")
        return
      }
      if amount < 0 {
        send_help(s, m.ChannelID, fmt.Sprintf(
          "Nice try %v",
          m.Author.Mention(),
        ))
        return
      }
      if target.ID == m.Author.ID {
        send_help(s, m.ChannelID, fmt.Sprintf(
          "Why would you do that %v",
          m.Author.Mention(),
        ))
        return
      }
      pay_request(s, m.Author, target, coins(amount))
    // case "loan", "l":
    //   if len(args) != 3 || len(m.Mentions) != 1 {
    //     send_help(s, m.ChannelID, "Usage: loan <@user> <amount> <interest>")
    //     return
    //   }
    //   target := m.Mentions[0]
    //   if target.ID == m.Author.ID {
    //     send_help(s, m.ChannelID, fmt.Sprintf(
    //       "Why would you do that %v",
    //       m.Author.Mention(),
    //     ))
    //     return
    //   }
    //   amount, err := strconv.ParseInt(args[1], 0, 64)
    //   if err != nil {
    //     send_help(s, m.ChannelID, "Usage: loan <@user> <amount> <interest>")
    //     return
    //   }
    //   // We want a % sign at the end
    //   if len(args[2]) < 1 || args[2][len(args[2])-1] != '%' {
    //     send_help(s, m.ChannelID, "Usage: loan <@user> <amount> <interest>")
    //     return
    //   }
    //   // Pop off the percent sign when we parse float
    //   interest_percent, err := strconv.ParseFloat(args[2][:len(args[2])-1], 64)
    //   if err != nil {
    //     send_help(s, m.ChannelID, "Usage: loan <@user> <amount> <interest>")
    //     return
    //   }
    //   interest := interest_percent * 0.01
    //   if interest < INTEREST_PER_HOUR {
    //     s.ChannelMessageSend(m.ChannelID, fmt.Sprintf(
    //       "I'm sorry %v, you cannot give a loan that has a lower interest than your own interest",
    //       m.Author.Mention(),
    //     ))
    //     return
    //   }
    //   if interest > 0.5 {
    //     s.ChannelMessageSend(m.ChannelID, fmt.Sprintf(
    //       "I'm sorry %v, you cannot give a loan that has a higher interest than 50%%",
    //       m.Author.Mention(),
    //     ))
    //     return
    //   }
    //   msg, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf(
    //     "%v, are you sure you want to loan %v %v, at a %v%% interest rate? (both users must accept)",
    //     m.Author.Mention(),
    //     target.Mention(),
    //     coins(amount),
    //     interest_percent,
    //   ))
    //   if err != nil {
    //     fmt.Println("ERROR: Could not send message:", err)
    //     return
    //   }
    //   s.MessageReactionAdd(msg.ChannelID, msg.ID, ACCEPT_ID)
    //   s.MessageReactionAdd(msg.ChannelID, msg.ID, DECLINE_ID)
    //   pending_lock.Lock()
    //   pending[msg.ID] = &pending_transaction{
    //     t: Transaction_LOAN,
    //     from: m.Author,
    //     to: target,
    //     amount: coins(amount),
    //     interest: 1 + interest,
    //   }
    //   pending_lock.Unlock()
    case "interest":
      // if m.Author.ID != "343545158140428289" {
      //   return
      // }
      if m.Author.ID != "300058406364905474" {
        return
      }
      if len(args) != 1 {
        send_help(s, m.ChannelID, "Usage: interest <percent>")
        return
      }
      percent, err := strconv.ParseFloat(args[0], 64)
      if err != nil {
        send_help(s, m.ChannelID, "Usage: interest <percent>")
        return
      }
      msgs := interest(percent * 0.01)
      for _, msg := range msgs {
        s.ChannelMessageSend(m.ChannelID, msg)
      }
    case "add":
      if m.Author.ID != "343545158140428289" {
        return
      }
      // if m.Author.ID != "300058406364905474" {
      //   return
      // }
      if len(args) < 1 || len(args) > 2 {
        send_help(s, m.ChannelID, "Usage: add [@user] <amount>")
        return
      }
      target := m.Author
      // Last arg is amount
      amount, err := strconv.ParseInt(args[len(args)-1], 10, 64)
      if err != nil {
        send_help(s, m.ChannelID, "Usage: add [@user] <amount>")
        return
      }
      if len(args) == 2 {
        if len(m.Mentions) != 1 {
          send_help(s, m.ChannelID, "Usage: add [@user] <amount>")
          return
        }
        target = m.Mentions[0]
      }
      new_amount := add_bal(target.ID, coins(amount))
      s.ChannelMessageSend(m.ChannelID, fmt.Sprintf(
        "Added %v to %v's balance, making their balance %v",
        coins(amount),
        target.Mention(),
        new_amount,
      ))
    }
  })
  go setup_interest(s)
}

func setup_interest(s *discordgo.Session) {
  // Disabled for now
  return

  // This is the time at the start of the next hour
  t := time.Now().Truncate(INTEREST_INTERVAL).Add(INTEREST_INTERVAL)
  // This waits until the top of the hour
  time.Sleep(time.Until(t))
  // Now that we've hit the hour, we give interest
  msgs := interest(INTEREST_PER_HOUR)
  for _, msg := range msgs {
    s.ChannelMessageSend(TRANSACTIONS_CHANNEL, msg)
  }
  ticker := time.NewTicker(INTEREST_INTERVAL)
  for range ticker.C {
    // This is close enough to the hour, that we can just use this for now.
    msgs := interest(INTEREST_PER_HOUR)
    for _, msg := range msgs {
      s.ChannelMessageSend(TRANSACTIONS_CHANNEL, msg)
    }
  }
}
