package coins

import (
  "fmt"
  "time"

  f "github.com/fauna/faunadb-go/v3/faunadb"
)

type UserAmount struct {
  User string `fauna:"user"`
  Amount coins `fauna:"amount"`
}

type coins int

func (c coins) String() string {
  return fmt.Sprintf("%dcc", c)
}

// To find the total, use this:
// Sum(Map(Paginate(Match(Index("amounts"))), Lambda("x", Select(0, Var("x")))))

var (
  // Map users to amounts
  // bals = make(map[string]coins)
  // bals_lock = &sync.Mutex{}
  // Map of loans by user id to list of loans they have recieved.
  // loans_received = make(map[string][]*outstanding_loan)
  // Map of loans by user id to list of loans they have sent.
  // loans_sent = make(map[string][]*outstanding_loan)
  // loans_lock = &sync.Mutex{}
)

// This retrieves the balance of id from faunadb. If the user does not have a balance, it
// will be created in the table.
func get_bal(id string) coins {
  res, err := fauna_client.Query(
    f.Let().
    Bind("user", f.MatchTerm(f.Index("users"), id)).
    In(f.If(
      f.Exists(f.Var("user")),
      f.Get(f.Var("user")),
      f.Create(f.Collection("balances"), f.Obj{
        "data": f.Obj{
          "user": id,
          "amount": STARTING_AMOUNT,
        },
      }),
    ),
  ))
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT
  }
  amount := STARTING_AMOUNT
  err = res.At(f.ObjKey("data", "amount")).Get(&amount)
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT
  }
  return amount
}

// This is an admin command. It adds amount to the users balance. Amount can be
// negative.
func add_bal(id string, amount coins) coins {
  res, err := fauna_client.Query(
    f.Let().
    Bind("user", f.MatchTerm(f.Index("users"), id)).
    Bind("total", f.Ref(f.Collection("total"), "299451389842555402")).
    In(f.If(
      f.Exists(f.Var("user")),
      f.Do(
        f.Update(f.Var("total"), f.Obj{
          "data": f.Obj{
            "total": f.Add(f.Select(f.Arr{"data", "total"}, f.Get(f.Var("total"))), amount),
          },
        }),
        f.Update(f.Select("ref", f.Get(f.Var("user"))), f.Obj{
          "data": f.Obj{
            "amount": f.Add(f.Select(f.Arr{"data", "amount"}, f.Get(f.Var("user"))), amount),
          },
        }),
      ),
      f.Create(f.Collection("balances"), f.Obj{
        "data": f.Obj{
          "user": id,
          "amount": STARTING_AMOUNT + amount,
        },
      }),
    ),
  ))
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT
  }
  err = res.At(f.ObjKey("data", "amount")).Get(&amount)
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT
  }
  return amount
}

// This returns the total amount of colacoin in circulation.
func get_total() coins {
  res, err := fauna_client.Query(
    f.Get(f.Ref(f.Collection("total"), "299451389842555402")),
  )
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT
  }
  amount := coins(0)
  err = res.At(f.ObjKey("data", "total")).Get(&amount)
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT
  }
  return amount
}

// This retrieves the balance of id from faunadb. If the value doesn't exist, this will return
// the STARTING_AMOUNT, and false. It won't write anything to fauna if the value doesn't exist.
func get_noset(id string) (coins, bool) {
  res, err := fauna_client.Query(
    f.Let().
    Bind("user", f.MatchTerm(f.Index("users"), id)).
    In(f.If(
      f.Exists(f.Var("user")),
      f.Get(f.Var("user")),
      nil,
    ),
  ))
  if err != nil {
    fmt.Println("ERROR:", err)
    return STARTING_AMOUNT, false
  }
  amount := STARTING_AMOUNT
  // Whenever the id doesn't exist, err will be non-nil
  err = res.At(f.ObjKey("data", "amount")).Get(&amount)
  if err != nil {
    return STARTING_AMOUNT, false
  }
  return amount, true
}

// Returns "" if the payment succeeded, or a discord message if otherwise.
func pay(from, to string, amount coins) (string, bool) {
  res, err := fauna_client.Query(
    f.Let().
    Bind("from", f.Get(f.MatchTerm(f.Index("users"), from))).
    Bind("from_ref", f.Select("ref", f.Var("from"))).
    Bind("to", f.Get(f.MatchTerm(f.Index("users"), to))).
    Bind("to_ref", f.Select("ref", f.Var("to"))).
    In(f.If(
      f.LTE(f.Select(f.Arr{"data", "amount"}, f.Var("from")), amount),
      f.Format(
        "%s %dcc",
        fmt.Sprintf(
          "<@%v>, you are too poor! You tried to pay %v, but you only have",
          from,
          amount,
        ),
        f.Select(f.Arr{"data", "amount"}, f.Var("from")),
      ),
      f.Do(
        f.Update(f.Var("from_ref"), f.Obj{
          "data": f.Obj{
            "amount": f.Subtract(f.Select(f.Arr{"data", "amount"}, f.Var("from")), amount),
          },
        }),
        f.Update(f.Var("to_ref"), f.Obj{
          "data": f.Obj{
            "amount": f.Add(f.Select(f.Arr{"data", "amount"}, f.Var("to")), amount),
          },
        }),
      ),
    )),
  )
  if err != nil {
    fmt.Println("ERROR:", err)
    return "", false
  }
  msg := ""
  err = res.Get(&msg)
  if err != nil {
    return fmt.Sprintf(
      "<@%v> paid <@%v> %v",
      from,
      to,
      amount,
    ), true
  } else {
    return msg, false
  }
}

// This prints target's balance nicely.
func show_bal(id string) string {
  return fmt.Sprintf(
    "<@%v>'s balance: %v",
    id,
    get_bal(id),
  )
}

// This retrieves the top users from faunadb.
func get_top() []UserAmount {
  res, err := fauna_client.Query(f.Map(
    f.Paginate(f.Match(f.Index("amounts")), f.Size(10)),
    f.Lambda(
      "x",
      f.Select(f.Arr{"data"}, f.Get(f.Select(1, f.Var("x")))),
    ),
  ))
  if err != nil {
    fmt.Println("ERROR:", err)
    return nil
  }
  items := make([]UserAmount, 10)
  err = res.At(f.ObjKey("data")).Get(&items)
  if err != nil {
    fmt.Println("ERROR:", err)
    return nil
  }
  return items
}

// Gives interest to all users. Amount is a percentage in float form. So 2%
// interest should be 0.02. Returns a list of discord messages.
func interest(amount float64) []string {
  _, err := fauna_client.Query(f.Map(
    f.Paginate(f.Match(f.Index("amounts"))),
    f.Lambda(
      "x",
      f.Let().
      Bind("amount", f.Select(0, f.Var("x"))).
      Bind("ref", f.Select(1, f.Var("x"))).
      In(f.Update(
        f.Var("ref"),
        f.Obj{
          "data": f.Obj{
            "amount": f.Ceil(f.Multiply(f.Var("amount"), 1 + amount)),
          },
        },
      )),
    ),
  ))
  if err != nil {
    fmt.Println("ERROR:", err)
    return nil
  }
  strs := []string{}
  strs = append(strs, fmt.Sprintf(
    "Gave everyone %.2f%% interest",
    amount * 100,
  ))
  return strs
}

// Creates a voucher for the given user. This will remove cc from their account,
// and create a new voucher in the vouchers table.
//
// Returns the amount in the users balance, and if the voucher was actually created.
// If something goes wrong, then it will return (0, false).
func create_voucher(user string, voucher string, amount int) (int, bool) {
  res, err := fauna_client.Query(
    f.Let().
    Bind("user", f.Get(f.MatchTerm(f.Index("users"), user))).
    Bind("user_ref", f.Select("ref", f.Var("user"))).
    In(f.If(
      f.LTE(f.Select(f.Arr{"data", "amount"}, f.Var("user")), amount),
      f.Select(f.Arr{"data", "amount"}, f.Var("user")),
      f.Do(
        f.Update(f.Var("user_ref"), f.Obj{
          "data": f.Obj{
            "amount": f.Subtract(f.Select(f.Arr{"data", "amount"}, f.Var("user")), amount),
          },
        }),
        f.Create(f.Collection("vouchers"), f.Obj{
          "data": f.Obj{
            "created_at": time.Now().Unix(),
            "created_by": f.Var("user_ref"),
            "voucher": voucher,
            "amount": amount,
          },
        }),
      ),
    )),
  )
  if err != nil {
    fmt.Println("ERROR:", err)
    return 0, false
  }
  bal := 0
  err = res.Get(&bal)
  return bal, err != nil
}

// Redeems a voucher, and gives the amount in the voucher to the user.
//
// This will return true if there was a valid voucher, and false if
// there was an error, or if the voucher was invalid.
func redeem_voucher(user string, voucher string) bool {
  res, err := fauna_client.Query(
    f.Let().
    Bind("user", f.Get(f.MatchTerm(f.Index("users"), user))).
    Bind("user_ref", f.Select("ref", f.Var("user"))).
    Bind("voucher_ref", f.MatchTerm(f.Index("vouchers"), voucher)).
    In(f.If(
      f.Equals(f.Count(f.Var("voucher_ref")), 1),
      f.Let(). // Means the voucher pointed to a valid ref
      Bind("voucher", f.Get(f.Var("voucher_ref"))).
      In(f.Do(
        f.Update(f.Var("user_ref"), f.Obj{
          "data": f.Obj{
            "amount": f.Add(
              f.Select(f.Arr{"data", "amount"}, f.Var("user")),
              f.Select(f.Arr{"data", "amount"}, f.Var("voucher")),
            ),
          },
        }),
        f.Delete(f.Select("ref", f.Var("voucher"))),
        f.BooleanV(true), // Valid ref, we return true
      )),
      f.BooleanV(false), // Invalid ref, we do nothing here
    )),
  )
  if err != nil {
    fmt.Println("ERROR:", err)
    return false
  }
  worked := false
  res.Get(&worked)
  return worked
}

// // This does not lock bals!
// func add_bal(id string, amount coins) bool {
//   new_amount := get_bal(id) + amount
//   if new_amount < 1 {
//     return false
//   }
//   bals[id] = new_amount
//   return true
// }
//
//
// // This locks loans, and returns a discord message showing that users sent and received loans.
// func show_loans(target *discordgo.User) *discordgo.MessageEmbed {
//   loans_lock.Lock()
//   defer loans_lock.Unlock()
//   e := &discordgo.MessageEmbed{}
//   e.Type = discordgo.EmbedTypeRich
//   e.Title = fmt.Sprintf(
//     "Loans for %v",
//     target,
//   )
//   str := ""
//   for _, l := range loans_sent[target.ID] {
//     u := &discordgo.User{ID: l.to}
//     str += fmt.Sprintf(
//       "%v needs to pay you %v (%.2f%% interest)\n",
//       u.Mention(),
//       l.amount,
//       (l.interest - 1) * 100,
//     )
//   }
//   if str != "" {
//     e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
//       Name: "Sent loans",
//       Value: str,
//     })
//   }
//   str = ""
//   for _, l := range loans_received[target.ID] {
//     u := &discordgo.User{ID: l.from}
//     str += fmt.Sprintf(
//       "You must pay %v %v (%.2f%% interest)\n",
//       u.Mention(),
//       l.amount,
//       (l.interest - 1) * 100,
//     )
//   }
//   if str != "" {
//     e.Fields = append(e.Fields, &discordgo.MessageEmbedField{
//       Name: "Received loans",
//       Value: str,
//     })
//   }
//   return e
// }
