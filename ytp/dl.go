package ytp

import (
  "fmt"
  "log"
  "errors"
  "regexp"
  "strings"
  "encoding/json"

  "gitlab.com/macmv/cola-bot/util"
)

var (
  // This is an info map for the all the video metadata
  reg_player_response = regexp.MustCompile(`ytInitialPlayerResponse\s*=\s*({.+?})\s*;`)
  // Matches json until a semicolon
  reg_json = regexp.MustCompile(`({.+?});`)
  // Matches a video id in a url
  reg_video_id = regexp.MustCompile(`v=[A-Za-z0-9-_]+`)
)

type Video struct {
  id string
  info map[string]interface{}
}

// This searches youtube for the given search terms,
// and returns a video id.
func Search(term string) string {
  return ""
}

// This gets a video from a url.
// This does not fetch any data,
// you must call Video.Download*
// functions in order to download
// anything. This will search for the
// video if the url does not start
// with https://.
func GetVideo(url string) (*Video, error) {
  v := &Video{}
  if strings.HasPrefix(url, "https:/") {
    // Direct video url
    path := url[8:]
    if strings.HasPrefix(path, "youtu.be/") {
      v.id = path[9:]
      if len(v.id) == 0 {
        return nil, errors.New("Cannot find video id in url")
      }
    } else {
      v.id = reg_video_id.FindString(url)
      if len(v.id) == 0 {
        return nil, errors.New("Cannot find video id in url")
      }
      // Remove the v= at the start
      v.id = v.id[2:]
    }
  } else {
    // Search for the video
    v.id = search_for_video(url)
  }
  return v, nil
}

// This gets metadata like the date uploaded,
// and the title of the video. This function
// will cache the info, so calling it multiple
// times will only ever access the network once.
// Call RefreshInfo to refresh this cache.
func (v *Video) Info() map[string]interface{} {
  if v.info == nil {
    v.RefreshInfo()
  }
  return v.info
}

// This gets the video info, and always accesses
// the network. There is little reason to call
// this function (Info() will cache the result,
// and only access the network on the first call)
func (v *Video) RefreshInfo() map[string]interface{} {
  body, err := util.DownloadPage(fmt.Sprintf(
    "https://www.youtube.com/watch?v=%v&gl=US&hl=en&has_verified=1&bpctr=9999999999",
    v.id,
  ))
  if err != nil { log.Println(err); return nil }

  match := reg_player_response.Find(body)
  if len(match) == 0 { return nil }

  match_json := reg_json.Find(match)
  // Remove semicolon
  match_json = match_json[:len(match_json)-1]

  v.info = make(map[string]interface{})
  err = json.Unmarshal(match_json, &v.info)
  if err != nil { log.Println(err); return nil }

  return v.info
}

// This returns the url to an audio file.
func (v *Video) AudioURL(sample_rate int) string {
  info := v.Info()
  streaming_data := info["streamingData"].(map[string]interface{})
  formats := streaming_data["adaptiveFormats"].([]interface{})

  min_bitrate := -1
  url := ""
  for _, i := range formats {
    f := i.(map[string]interface{})
    if strings.HasPrefix(f["mimeType"].(string), "audio") && f["audioSampleRate"] == fmt.Sprint(sample_rate) {
      bitrate := int(f["bitrate"].(float64))
      new_url, ok := f["url"].(string)
      if ok && (bitrate < min_bitrate || min_bitrate == -1) {
        min_bitrate = bitrate
        url = new_url
      }
    }
  }
  return url
}

// This returns a url to the video,
// in the format https://youtu.be/<id>
func (v *Video) URL() string {
  return fmt.Sprintf("https://youtu.be/%v", v.id)
}

// This returns the video id
func (v *Video) ID() string {
  return v.id
}
