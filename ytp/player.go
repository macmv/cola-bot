package ytp

import (
  "os"
  "fmt"
  "log"
  "bytes"
  "os/exec"
  "strings"
  "net/http"

  "github.com/bwmarrin/discordgo"

  "gitlab.com/macmv/cola-bot/audio"
)

var (
  queues = make(map[string]*queue)
)

type queue struct {
  // List of youtube urls
  videos []*Video
  // Send an empty struct to this channel to skip a song
  skip chan struct{}
  // Set to true to loop the some at the start of the queue
  loop bool
}

// This adds a handler to the given discord session, which will look for messages
// sent that start with ~play
func AddHandlers(s *discordgo.Session) {
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
    // Ignore bot messages
    if m.Author.Bot {
      return
    }
    if strings.HasPrefix(m.Content, "~p ") || strings.HasPrefix(m.Content, "~play ") {
      var arg string
      if strings.HasPrefix(m.Content, "~p ") {
        arg = m.Content[len("~p "):]
      } else if strings.HasPrefix(m.Content, "~play ") {
        arg = m.Content[len("~play "):]
      }

      // Checks if the user that sent !play is in a voice channel
      vc, err := s.State.VoiceState(m.GuildID, m.Author.ID)
      // This will return if the user is not in vc
      if err != nil {
        s.ChannelMessageSend(m.ChannelID, "Please join a voice channel to use ~play")
        return
      }

      if !strings.HasPrefix(arg, "https:/") {
        // arg = "ytsearch:" + arg
      }
      log.Println("Searching for video " + arg + "...")
      v, err := GetVideo(arg)
      if err != nil {
        s.ChannelMessageSend(m.ChannelID, "Error while fetching video: " + err.Error())
        return
      }
      s.ChannelMessageSend(m.ChannelID, "Playing video " + v.URL())

      _, ok := queues[m.GuildID]
      if !ok {
        // New song, need to create queue
        queues[m.GuildID] = &queue{
          videos: []*Video{v},
          skip: make(chan struct{}),
        }
        // Need to start player for this guild
        go start_player(s, m.GuildID, vc.ChannelID)
      } else {
        // Already playing, need to add to queue
        queues[m.GuildID].videos = append(queues[m.GuildID].videos, v)
      }
    } else if strings.HasPrefix(m.Content, "~d") ||
    strings.HasPrefix(m.Content, "~disconnect") ||
    strings.HasPrefix(m.Content, "~leave") {
      _, ok := s.VoiceConnections[m.GuildID]
      if !ok {
        s.ChannelMessageSend(m.ChannelID, "I am not playing anything right now!")
        return
      }

      delete(queues, m.GuildID)
      skip(m.GuildID)
    } else if strings.HasPrefix(m.Content, "~s") ||
    strings.HasPrefix(m.Content, "~skip") {
      _, ok := s.VoiceConnections[m.GuildID]
      if !ok {
        s.ChannelMessageSend(m.ChannelID, "I am not playing anything right now!")
        return
      }

      skip(m.GuildID)
    } else if strings.HasPrefix(m.Content, "~l") ||
    strings.HasPrefix(m.Content, "~loop") {
      _, ok := s.VoiceConnections[m.GuildID]
      if !ok {
        s.ChannelMessageSend(m.ChannelID, "I am not playing anything right now!")
        return
      }

      queues[m.GuildID].loop = !queues[m.GuildID].loop

      if queues[m.GuildID].loop {
        s.ChannelMessageSend(m.ChannelID, "Loop enabled")
      } else {
        s.ChannelMessageSend(m.ChannelID, "Loop disabled")
      }
    } else if strings.HasPrefix(m.Content, "~q") ||
    strings.HasPrefix(m.Content, "~queue") {
      _, ok := s.VoiceConnections[m.GuildID]
      if !ok {
        s.ChannelMessageSend(m.ChannelID, "I am not playing anything right now!")
        return
      }

      e := &discordgo.MessageEmbed{}
      e.Title = "Queue"
      e.Type = discordgo.EmbedTypeRich
      for i, v := range queues[m.GuildID].videos {
        f := &discordgo.MessageEmbedField{}
        f.Name = fmt.Sprintf("%v.", i+1)
        f.Value = v.URL()
        f.Inline = false
        e.Fields = append(e.Fields, f)
      }
      s.ChannelMessageSendEmbed(m.ChannelID, e)
    }
  })
}

func skip(guild string) {
  queues[guild].skip <- struct{}{}
}

func start_player(s *discordgo.Session, guild, channel string) {
  // Connect to vc
  vc, err := s.ChannelVoiceJoin(guild, channel, false, false)
  if err != nil {
    log.Println("Failed to join channel:", err)
    return
  }

  for {
    if len(queues[guild].videos) > 0 {
      // Get first element in queue
      v := queues[guild].videos[0]
      // Play that song (blocking call)
      play_url(vc, v.AudioURL(48000))
      if !queues[guild].loop {
        // Remove it, if we don't want to loop
        queues[guild].videos = queues[guild].videos[1:]
      }
    } else {
      // Empty queue, we clear it
      delete(queues, guild)
      break
    }
  }

  vc.Disconnect()
}

// This plays a url based on an audio direct url.
// This is passed through ffpmeg, so almost any format
// is accepted.
func play_url(vc *discordgo.VoiceConnection, url string) error {
  res, err := http.Get(url)
  if err != nil {
    log.Println("Error while getting audio: ", err)
    return err
  }
  defer res.Body.Close()

  var wav bytes.Buffer

  // '-i -' means use stdin as the input data
  // '-f wav' means output in wav format
  // '-' means output to stdout
  ffmpeg_cmd := exec.Command("/usr/bin/ffmpeg", "-i", "-", "-f", "wav", "-")
  // The file we are downloading is some audio,
  // and ffmpeg will handle finding the type of audio.
  ffmpeg_cmd.Stdin = res.Body
  ffmpeg_cmd.Stdout = &wav
  // For debug
  ffmpeg_cmd.Stderr = os.Stdout

  log.Println("Downloading video...")
  ffmpeg_cmd.Run()
  log.Println("Done downloading")

  // Run this async, as this is a thread which will download the whole video,
  // and then play it back in discord. That will take a few minutes, and the
  // bot should be able to do other things during that time.
  audio.Play(vc, audio.WavSampler(&wav), queues[vc.GuildID].skip)
  return nil
}
