package ytp

import (
  "log"
  "encoding/json"

  "gitlab.com/macmv/cola-bot/util"
)

type search_result struct {
  Contents struct {
    TwoColumnSearchResultsRenderer struct {
      PrimaryContents struct {
        SectionListRenderer struct {
          Contents []struct {
            ItemSectionRenderer struct {
              Contents []struct {
                VideoRenderer struct {
                  VideoId string
                }
              }
            }
          }
        }
      }
    }
  }
}

func search_for_video(query string) string {
  data := map[string]interface{}{
    "context": map[string]interface{}{
      "client": map[string]interface{}{
        "clientName": "WEB",
        "clientVersion": "2.20201021.03.00",
      },
    },
    "query": query,
  }
  j, _ := json.Marshal(data)
  s, err := util.DownloadPOST(
    // Key from youtube-dl
    "https://www.youtube.com/youtubei/v1/search?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8",
    "application/json",
    string(j),
  )
  if err != nil { log.Println(err); return "" }

  var out search_result
  json.Unmarshal(s, &out)

  log.Println(out)

  sections := out.Contents.TwoColumnSearchResultsRenderer.PrimaryContents.SectionListRenderer.Contents
  if len(sections) == 0 { log.Println("Did not recieve any search results"); return "" }
  videos := sections[0].ItemSectionRenderer.Contents
  for _, v := range videos {
    // Channels can appear in this list, and we want to skip those
    if v.VideoRenderer.VideoId != "" {
      return v.VideoRenderer.VideoId
    }
  }
  log.Println("Did not recieve any search results")
  return ""
}
