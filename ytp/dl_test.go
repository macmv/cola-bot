package ytp_test

import (
  "log"
  "testing"

  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/cola-bot/ytp"
)

func TestVideoInfo(t *testing.T) {
  // youtube-dl test video
  v, _ := ytp.GetVideo("BaW_jenozKc")
  info := v.Info()

  details := info["videoDetails"].(map[string]interface{})
  assert.Equal(t, "BaW_jenozKc", details["videoId"], "The details should be correct")
  assert.Equal(t, "youtube-dl test video \"'/\\ä↭𝕐", details["title"], "The details should be correct")
  assert.Equal(t, []interface{}{"youtube-dl"}, details["keywords"], "The details should be correct")
  assert.Equal(t, "Philipp Hagemeister", details["author"], "The details should be correct")
  assert.Equal(t, "UCLqxVugv74EIW3VWh2NOa3Q", details["channelId"], "The details should be correct")
  assert.Equal(t, "10", details["lengthSeconds"], "The details should be correct")

  streaming_data := info["streamingData"].(map[string]interface{})
  formats := streaming_data["adaptiveFormats"].([]interface{})

  log.Println("Formats:")
  for _, i := range formats {
    f := i.(map[string]interface{})
    log.Printf("%v (%v) (%v)\n", f["quality"], f["qualityLabel"], f["mimeType"])
  }
}

func TestParseURL(t *testing.T) {
  v, err := ytp.GetVideo("https://www.youtube.com/watch?v=BaW_jenozKc")
  assert.Nil(t, err, "Creating a video should not cause an error")
  assert.Equal(t, "BaW_jenozKc", v.ID(), "The video should have the correct id")

  v, err = ytp.GetVideo("https://youtube.com/watch?v=BaW_jenozKc")
  assert.Nil(t, err, "Creating a video should not cause an error")
  assert.Equal(t, "BaW_jenozKc", v.ID(), "The video should have the correct id")

  v, err = ytp.GetVideo("https://youtu.be/BaW_jenozKc")
  assert.Nil(t, err, "Creating a video should not cause an error")
  assert.Equal(t, "BaW_jenozKc", v.ID(), "The video should have the correct id")
}

func TestSearch(t *testing.T) {
  v, err := ytp.GetVideo("Gaming")
  assert.Nil(t, err, "Creating a video should not cause an error")
  assert.Greater(t, len(v.ID()), 0, "Creating a video should not cause an error")
  log.Println(v)
}
