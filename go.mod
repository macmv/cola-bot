module gitlab.com/macmv/cola-bot

go 1.15

require (
	github.com/aws/aws-sdk-go v1.37.30 // indirect
	github.com/bwmarrin/discordgo v0.23.2
	github.com/fauna/faunadb-go/v3 v3.0.0 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/sys v0.0.0-20210313202042-bd2e13477e9c // indirect
	gopkg.in/hraban/opus.v2 v2.0.0-20201025103112-d779bb1cc5a2
)
