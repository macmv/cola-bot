package role_storage

import (
  "os"
  "fmt"

  "github.com/bwmarrin/discordgo"
)

func AddHandlers(s *discordgo.Session) {
  s.AddHandler(func(s *discordgo.Session, m *discordgo.GuildMemberUpdate) {
    // Called whenever someone gets a new role
    err := os.MkdirAll("roles", 0755)
    if err != nil {
      fmt.Println("Error creating roles directory:", err)
      return
    }
    filename := "roles/" + m.User.ID + ".txt"
    file, err := os.Create(filename)
    if err != nil {
      fmt.Println("Error creating roles file:", err)
      return
    }
    defer file.Close()
    member, err := s.GuildMember(m.GuildID, m.User.ID)
    if err != nil {
      fmt.Println("Error fetching guild member: " + err.Error())
      return
    }
    for _, role := range member.Roles {
      file.Write([]byte(role + "\n"))
    }
    fmt.Println("Member ", m.User.Username, " has updated their roles! Saved roles to ", filename)
  })
}

