import discord
import asyncio
import time
import importlib
import random
import traceback

if __name__ == '__main__':
    intents = discord.Intents.default()
    intents.members = True
    intents.messages = True

    client = discord.Client(intents=intents)

    @client.event
    async def on_ready():
        print('Logged in as')
        print(client.user.name)

        # cola = client.get_guild(803542061977960448)
        # await cola.get_role(818751905516748822).delete()

    file = open('token.txt', mode='r')
    token = file.read()
    file.close()

    client.run(token)
