package util_commands

import (
  "strings"

  "gitlab.com/macmv/cola-bot/util"

  "github.com/bwmarrin/discordgo"
)

func AddHandlers(s *discordgo.Session) {
  add_roles_handlers(s)
  add_no_one_enters_handlers(s)
}

func add_roles_handlers(s *discordgo.Session) {
  s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
    if strings.HasPrefix(m.Content, "!roles") {
      bot_commands, err := util.BotCommands(s, m.GuildID)
      if err != nil {
        s.ChannelMessageSend(m.ChannelID, "Error while creating bot-commands channel: " + err.Error())
        return
      }
      if m.ChannelID != bot_commands.ID {
        s.ChannelMessageSend(m.ChannelID, "Please only use this command in " + bot_commands.Mention())
        return
      }
      params := strings.Split(m.Content, " ")
      if len(params) != 2 || len(m.MentionRoles) != 1 {
        s.ChannelMessageSend(m.ChannelID, "Usage: !roles <@role>")
        return
      }
      role := m.MentionRoles[0]
      members, err := s.GuildMembers(m.GuildID, "", 1000)
      if err != nil {
        s.ChannelMessageSend(m.ChannelID, "Error fetching members: " + err.Error())
        return
      }
      list := []string{}
      for _, u := range members {
        for _, r := range u.Roles {
          if r == role {
            list = append(list, u.User.Mention())
          }
        }
      }
      s.ChannelMessageSend(m.ChannelID, "Users with that role: \n" + strings.Join(list, "\n"))
    }
  })
}
