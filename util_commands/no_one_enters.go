package util_commands

import (
  "fmt"
  "time"
  "sync"

  "github.com/bwmarrin/discordgo"
)

var (
  cancel_lock *sync.Mutex = &sync.Mutex{}
  cancels map[string]chan struct{} = make(map[string]chan struct{})
)

func add_no_one_enters_handlers(s *discordgo.Session) {
  s.AddHandler(func(s *discordgo.Session, u *discordgo.VoiceStateUpdate) {
    c, err := s.Channel(u.VoiceState.ChannelID)
    if err != nil {
      fmt.Println(err)
      return
    }
    if c.Name == "no one enters" {
      cancel_lock.Lock()
      cancel, ok := cancels[u.UserID]
      if !ok {
        cancel = make(chan struct{})
        cancels[u.UserID] = cancel
      }
      cancel_lock.Unlock()
      go func(guild, user string, before *discordgo.VoiceState, cancel chan struct{}) {
        select {
        case <- time.After(5 * time.Second):
          if before == nil {
            // Disconnect user
            s.GuildMemberMove(guild, user, nil)
          } else {
            s.GuildMemberMove(guild, user, &before.ChannelID)
          }
          cancel_lock.Lock()
          close(cancel)
          delete(cancels, user)
          cancel_lock.Unlock()
        case <- cancel:
        }
      }(u.GuildID, u.UserID, u.BeforeUpdate, cancel)
    } else {
      cancel_lock.Lock()
      cancel, ok := cancels[u.UserID]
      if ok {
        cancel <- struct{}{}
        delete(cancels, u.UserID)
      }
      cancel_lock.Unlock()
    }
  })
}
