package util

import (
  "github.com/bwmarrin/discordgo"
)

var (
  bot_commands map[string]*discordgo.Channel = make(map[string]*discordgo.Channel)
)

func BotCommands(s *discordgo.Session, guild string) (*discordgo.Channel, error) {
  channel, ok := bot_commands[guild]
  if ok {
    return channel, nil
  }
  channels, err := s.GuildChannels(guild)
  if err != nil {
    return nil, err
  }
  for _, c := range channels {
    if c.Name == "bot-commands" {
      channel = c
      break
    }
  }
  if channel == nil {
    channel, err = s.GuildChannelCreate(guild, "bot-commands", discordgo.ChannelTypeGuildText)
    if err != nil {
      return nil, err
    }
  }
  bot_commands[guild] = channel
  return channel, nil
}
