package util

import (
  "io"
  "bytes"
  "strings"
  "net/http"
)

func DownloadPage(url string) ([]byte, error) {
  res, err := http.Get(url)
  if err != nil { return nil, err }
  defer res.Body.Close()

  var buf bytes.Buffer

  _, err = io.Copy(&buf, res.Body)
  if err != nil { return nil, err }

  return buf.Bytes(), nil
}

func DownloadPOST(url, content_type, data string) ([]byte, error) {
  res, err := http.Post(url, content_type, strings.NewReader(data))
  if err != nil { return nil, err }
  defer res.Body.Close()

  var buf bytes.Buffer

  _, err = io.Copy(&buf, res.Body)
  if err != nil { return nil, err }

  return buf.Bytes(), nil
}

