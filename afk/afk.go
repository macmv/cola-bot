package afk

import (
  "fmt"
  "time"
  "context"

  "github.com/bwmarrin/discordgo"
)

var (
  afk_channels map[string]*discordgo.Channel
  afk_move map[string]context.CancelFunc
  channels_moved_from map[string]string
)

func AddHandlers(s *discordgo.Session) {
  afk_channels = make(map[string]*discordgo.Channel)
  afk_move = make(map[string]context.CancelFunc)
  channels_moved_from = make(map[string]string)

  s.AddHandler(func(s *discordgo.Session, u *discordgo.VoiceStateUpdate) {
    user := u.VoiceState.UserID
    guild := u.VoiceState.GuildID
    channel := u.VoiceState.ChannelID
    set_afk(s, guild)
    if channel == afk_channels[guild].ID {
      if !(u.VoiceState.SelfDeaf || u.VoiceState.SelfMute) {
        prev_channel, ok := channels_moved_from[user]
        if ok {
          delete(channels_moved_from, user)
          s.GuildMemberMove(guild, user, &prev_channel)
        }
      }
      return
    }
    // People ended up muting in the call, and didn't want to
    // be moved to a different channel. This is why the mute
    // block is commented out.
    if u.VoiceState.SelfDeaf {
      auto_mute(s, guild, user, channel, 5 * time.Minute)
    // } else if u.VoiceState.SelfMute {
    //   auto_mute(guild, user, channel, 5 * time.Minute)
    } else {
      remove_mute(guild, user)
    }
  })
}

func auto_mute(s *discordgo.Session, guild, user, current_channel string, dur time.Duration) {
  cancel, ok := afk_move[user]
  if ok {
    cancel()
  }
  ctx, cancel := context.WithCancel(context.Background())
  afk_move[user] = cancel
  go func() {
    select {
    case <- time.After(dur):
      channel := afk_channels[guild].ID
      s.GuildMemberMove(guild, user, &channel)
      channels_moved_from[user] = current_channel
    case <- ctx.Done():
    }
  }()
}

func remove_mute(guild, user string) {
  cancel, ok := afk_move[user]
  if !ok { return }
  cancel()
  delete(afk_move, user)
}

func set_afk(s *discordgo.Session, guild string) {
  _, ok := afk_channels[guild]
  if ok { return }

  channels, err := s.GuildChannels(guild)
  if err != nil {
    fmt.Println(err)
    return
  }
  for _, c := range channels {
    if c.Name == "AFK" {
      afk_channels[guild] = c
      return
    }
  }
  fmt.Println("Could not find a channel named, creating...")
  afk_channels[guild], err = s.GuildChannelCreate(guild, "AFK", discordgo.ChannelTypeGuildVoice)
  if err != nil {
    fmt.Println("Error while creating channel:", err)
    return
  }
}

